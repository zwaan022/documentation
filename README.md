# Documentation

This project documents how the AFSG-Microbiology git repositories are organized and which guidelines can be followed for usage and access.
Please visit the [wiki](https://git.wur.nl/afsg-microbiology/documentation/-/wikis/home) for all relevant documentation.
